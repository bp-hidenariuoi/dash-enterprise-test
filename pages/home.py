import dash
from dash.dependencies import Input, Output, State
import dash_design_kit as ddk
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd
import plotly.express as px
import traceback

from app import app, snap
import pages


def create_page(temperature=None, pressure=None, humidity=None, output=None):
    return html.Div([
        ddk.ControlCard([
            ddk.ControlItem(dcc.Input(id='temperature', value=temperature), label='Temperature', width=30),
            ddk.ControlItem(dcc.Input(id='pressure', value=pressure), label='Pressure', width=30),
            ddk.ControlItem(dcc.Input(id='humidity', value=humidity), label='humidity', width=40),
            html.Button('Run', id='run'),
            html.Div(id='status')
        ], orientation='horizontal'),

        dcc.Store(id='snapshot-id'),

        ddk.Card([
            ddk.CardHeader(html.Button('Refresh', id='refresh'), title='Results'),
            html.Div(id='results', children=output)
        ])
    ])


def results(df, temperature=None, pressure=None, humidity=None):
    return html.Div([
        ddk.Graph(figure=px.scatter(df, x='sepal_length', y='sepal_width')),
        'Temperature: {}, Pressure: {}, Humidity: {}'.format(
            temperature, pressure, humidity
        )
    ])


def home_page_layout():
    return create_page()


def snapshot_layout(snapshot_id):
    snapshot = snap.snapshot_get(snapshot_id)
    df = pd.DataFrame(snapshot)
    temperature = snap.meta_get(snapshot_id, 'temperature')
    humidity = snap.meta_get(snapshot_id, 'humidity')
    pressure = snap.meta_get(snapshot_id, 'pressure')
    output = results(df, temperature, pressure, humidity)
    return create_page(
        temperature,
        humidity,
        pressure,
        output
    )


@app.callback(
    [Output('status', 'children'), Output('snapshot-id', 'data')],
    [Input('run', 'n_clicks')],
    [State('temperature', 'value'), State('pressure', 'value'), State('humidity', 'value')],
    prevent_initial_call=True)
def update_status(n_clicks, temperature, pressure, humidity):
    # Submit the long-running task to the task queue
    snapshot_id = snap.snapshot_save_async(
        run_model,
        temperature=temperature, pressure=pressure, humidity=humidity
    )
    # Save the model parameters so that they can be loaded later
    snap.meta_update(snapshot_id, dict(
        temperature=temperature, pressure=pressure, humidity=humidity
    ))
    return [
        # Update 5 to be however long you think this will take :)
        'Running! This takes about 5 minutes to complete',
        snapshot_id
    ]


@app.callback(
    Output('results', 'children'),
    [Input('refresh', 'n_clicks')],
    [State('snapshot-id', 'data')], prevent_initial_call=True)
def display_results(n_clicks, snapshot_id):
    try:
        # retrieve the data that was saved in the task
        snapshot = snap.snapshot_get(snapshot_id)
        temperature = snap.meta_get(snapshot_id, 'temperature')
        humidity = snap.meta_get(snapshot_id, 'humidity')
        pressure = snap.meta_get(snapshot_id, 'pressure')
    except Exception as e:
        traceback.print_exc()
        return 'Not ready yet'
    df = pd.DataFrame(snapshot)
    output = results(df, temperature, pressure, humidity)
    return output


@snap.celery_instance.task
@snap.snapshot_async_wrapper()
def run_model(temperature=None, pressure=None, humidity=None):
    # This function is called in a separate task queue managed by celery
    # This function's parameters (temperature, pressure, humidity) are
    # provided by the callback above with `snap.snapshot_save_async`

    # Whatever is returned by this function will be saved to the database
    # with the `snapshot_id`. It needs to be JSON-serializable

    # In this case, we're just returning a pandas dataframe
    df = px.data.iris()
    return df.to_dict()
